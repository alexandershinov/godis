package main

import (
	"github.com/go-redis/redis"
	"bitbucket.org/alexandershinov/godis"
	"fmt"
	"os"
)

func main() {
	var errorGetter bool
	for _, arg := range os.Args {
		if arg == "getErrors" {
			errorGetter = true
			break
		}
	}
	opt := &(redis.Options{
		Addr:     "localhost:6379", // Redis address
		Password: "",               // Redis password
		DB:       0,                // The number of redis database
	})
	if errorGetter {
		errors, err := godis.GetErrors(opt)
		if err != nil {
			panic(err)
		}
		for _, e := range errors {
			fmt.Println(e)
		}
	} else {
		client := godis.NewGodis(opt)
		fmt.Println(client.RedisKey)
		client.Start(0)
		fmt.Println("Role:", client.Role)
	}
	fmt.Println("To exit press any key...")
	fmt.Scanln()
}
