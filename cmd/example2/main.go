package main

import (
	"bitbucket.org/alexandershinov/godis"
	"github.com/go-redis/redis"
	"fmt"
)

func main() {
	opt := &(redis.Options{
		Addr:     "localhost:6379", // Redis address
		Password: "",               // Redis password
		DB:       0,                // The number of redis database
	})
	client := godis.NewGodis(opt)
	fmt.Println(client.RedisKey)
	client.Start(0)
	fmt.Println(client.Role)
	client2 := godis.NewGodis(opt)
	fmt.Println(client2.RedisKey)
	client2.Start(0)
	fmt.Println(client2.Role)
	fmt.Scanln()
	fmt.Println("GET ERRORS")
	errors, err := godis.GetErrors(opt)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(errors)
	}
}
