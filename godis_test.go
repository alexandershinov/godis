package godis_test

import (
	. "bitbucket.org/alexandershinov/godis"
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/go-redis/redis"
	"time"
	"sort"
	"fmt"
)

const (
	ThisInstanceKey = "self"
)

var opt = &(redis.Options{
	Addr:     "localhost:6379", // Redis address
	Password: "",               // Redis password
	DB:       0,                // The number of redis database
})

type newGodisTest struct {
	Opt *redis.Options
}

type godisUpdateTest struct {
	Opt              *redis.Options
	Role             int
	CurrentSenderKey string
}

type godisStartTest struct {
	Opt *redis.Options
}

type getErrorsTest struct {
	Opt        *redis.Options
	ErrorsList []string
}

func clearRedis(client *Godis) {
	client.RedisClient.Del("errors")
	client.RedisClient.Del("messages")
	client.RedisClient.Del("config:0:sender")
}

func (test *newGodisTest) Do(t *testing.T) {
	client := NewGodis(test.Opt)
	assert.NotEqual(t, client.RedisKey, "", "RedisKey is empty!")
	_, err := client.RedisClient.Ping().Result()
	assert.Equal(t, nil, err, "Check redis!!!")
}

func TestNewGodis(t *testing.T) {
	for name, test := range map[string]newGodisTest{
		"local": {
			Opt: opt,
		},
	} {
		t.Run(name, test.Do)
	}
}

func (test *godisUpdateTest) Do(t *testing.T) {
	client := NewGodis(test.Opt)
	if test.CurrentSenderKey == ThisInstanceKey {
		test.CurrentSenderKey = client.RedisKey
	}
	client.RedisClient.Set("config:0:sender", test.CurrentSenderKey, DefaultKeepASenderTimeout)
	client.UpdateType()
	assert.Equal(t, test.Role, client.Role, "Client choose bad role! %d != %d", test.Role, client.Role)
}

func TestGodis_UpdateType(t *testing.T) {
	for name, test := range map[string]godisUpdateTest{
		"isSender": {
			Opt:              opt,
			Role:             TypeSender,
			CurrentSenderKey: ThisInstanceKey,
		},
		"emptyCurrentSender": {
			Opt:              opt,
			Role:             TypeSender,
			CurrentSenderKey: "",
		},
		"isReceiver": {
			Opt:              opt,
			Role:             TypeReceiver,
			CurrentSenderKey: "1234567890",
		},
	} {
		t.Run(name, test.Do)
	}
}

func (test *godisStartTest) Do(t *testing.T) {
	client := NewGodis(test.Opt)
	clearRedis(client)
	// Test sender
	client.Start(0)
	client2 := redis.NewClient(opt)
	msg, err := client2.BLPop(5*time.Second, "messages").Result()
	client.Stop()
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(msg)
	assert.NotEqual(t, "", msg[1], "Message is empty!")
	time.Sleep(500 * time.Millisecond)
	// Test receiver
	clearRedis(client)
	client.RedisClient.Set("config:0:sender", "test", DefaultKeepASenderTimeout*3)
	client.RedisClient.RPush("messages", "test message")
	messagesCount, err := client.RedisClient.LLen("messages").Result()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, int64(1), messagesCount, "Send test message fail.")
	client.Start(0)
	defer client.Stop()
	t.Log("Please wait... Client should get the message.")
	time.Sleep(1 * time.Second)
	messagesCount, err = client.RedisClient.LLen("messages").Result()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, int64(0), messagesCount, "Messages list should be empty.")
}

func TestGodis_Start(t *testing.T) {
	for name, test := range map[string]godisStartTest{
		"simple": {
			Opt: opt,
		},
	} {
		t.Run(name, test.Do)
	}
}

func (test *getErrorsTest) Do(t *testing.T) {
	client := NewGodis(test.Opt)
	clearRedis(client)
	var errorsList []interface{}
	for _, v := range test.ErrorsList {
		errorsList = append(errorsList, v)
	}
	_, err := client.RedisClient.SAdd("errors", errorsList...).Result()
	if err != nil {
		t.Fatal(err)
	}
	errors, err := GetErrors(test.Opt)
	assert.Equal(t, len(test.ErrorsList), len(errors), "Amount of errors is bad: %d / %d", len(test.ErrorsList), len(errors))
	sort.Strings(test.ErrorsList)
	sort.Strings(errors)
	assert.Equal(t, test.ErrorsList, errors, "Errors list %v is not the same as %v", test.ErrorsList, errors)
}

func TestGetErrors(t *testing.T) {
	for name, test := range map[string]getErrorsTest{
		"simple": {
			Opt:        opt,
			ErrorsList: []string{"error 1", "error 2", "error 3"},
		},
	} {
		t.Run(name, test.Do)
	}
}
