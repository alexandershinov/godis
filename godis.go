package godis

import (
	"github.com/satori/go.uuid"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"math/rand"
	"time"
	"strconv"
	"flag"
)

const (
	// There are three types of Godis
	// TypeSender: the main instance that send messages
	TypeSender = 1
	// TypeReceiver: default instances (only one instance should be sending messages)
	TypeReceiver = 2
	// TypeCleaner: the instance of program that get errors from database, delete these errors and after that close itself
	TypeCleaner = 3
	// TypeNone: the instance of program that closed
	TypeNone = 0

	// Timeout value - to define when the main instance falls down
	DefaultKeepASenderTimeout time.Duration = 5 * time.Second
	DefaultMessageDelay       time.Duration = 500 * time.Millisecond
)

func init() {
	rand.Seed(time.Now().Unix())
	// Parce flag for glog
	flag.Parse()
}

// Just for return some custom errors
type GodisError struct {
	text string
}

func (godisError GodisError) Error() string {
	return godisError.text
}

// Godis structure define an element of message queues system
// Every instance of Godis has three fields:
type Godis struct {
	RedisKey    string        // unique random string like uuid
	Role        int           // type of instance
	RedisClient *redis.Client // redis client pointer
}

// Method to generate RedisKey
func (g *Godis) generateRedisKey() {
	g.RedisKey = strconv.FormatInt(time.Now().UTC().Unix(), 16) + "-" + uuid.NewV4().String()
}

// Function to create new instance of Godis with default or custom options
// Usage for default options: NewGodis(nil)
func NewGodis(opt *redis.Options) *Godis {
	if opt == nil {
		opt = &(redis.Options{
			Addr:     "localhost:6379", // Redis address
			Password: "",               // Redis password
			DB:       0,                // The number of redis database
		})
	}
	// Create empty instance
	g := Godis{}
	// Add redis client with opt
	g.RedisClient = redis.NewClient(opt)
	// Generate a key
	g.generateRedisKey()
	// Return address of new instance
	return &g
}

// Check this instance is sender or receiver
func (g *Godis) UpdateType() (err error) {
	// Check key isn't empty
	if g.RedisKey == "" {
		err = GodisError{"Redis Key shouldn't be empty!"}
	}
	// Get current sender key from Redis
	currentSender, _ := g.RedisClient.Get("config:0:sender").Result()
	// If it's empty or the same as RedisKey, set this instance as sender
	if currentSender == "" || currentSender == g.RedisKey {
		var result string
		result, err = g.RedisClient.Set("config:0:sender", g.RedisKey, DefaultKeepASenderTimeout).Result()
		if result == "OK" {
			g.Role = TypeSender
		} else {
			g.Role = TypeReceiver
		}

	} else {
		g.Role = TypeReceiver
	}
	return nil
}

// 5% of messages will be bad
func hasError(_ string) bool {
	if rand.Intn(20)%20 == 0 {
		return true
	}
	return false
}

// Method to start Godis worker
func (g *Godis) Start(MessageDelay time.Duration) (err error) {
	if MessageDelay == 0 {
		MessageDelay = DefaultMessageDelay
	}
	err = g.UpdateType()
	if err != nil {
		return
	}
	// Always keep sender routine
	go func() {
		for g.Role != TypeNone {
			g.UpdateType()
			time.Sleep(DefaultKeepASenderTimeout - 500*time.Millisecond)
		}
	}()
	go func() {
		for g.Role != TypeNone {
			if g.Role == TypeSender {
				// Send message
				message := "Message " + time.Now().String()
				g.RedisClient.RPush("messages", message)
				glog.Info("SEND: ", message)
				time.Sleep(MessageDelay)
			} else if g.Role == TypeReceiver {
				// Read message
				msg, err := g.RedisClient.BLPop(5*time.Second, "messages").Result()
				if err == nil {
					glog.Info("RECEIVED: ", msg[1])
					if hasError(msg[1]) {
						// Log error if message is bad
						glog.Info("ERROR")
						g.RedisClient.SAdd("errors", msg[1])
					}
				}
			}
		}
	}()
	return nil
}

func (g *Godis) Stop() {
	g.Role = TypeNone
}

// Clear error log function
func GetErrors(opt *redis.Options) (errors []string, err error) {
	// Create new Godis as cleaner (TypeCleaner)
	g := NewGodis(opt)
	g.Role = TypeCleaner
	// Get all errors from Redis step by step
	// Because if there are many errors in log, it might crash with tcp r/w timeout
	var errorsList []string
	errorsCount, err := g.RedisClient.SCard("errors").Result()
	var i int64
	var step int64 = 500
	for ; i < errorsCount; i += step {
		errors, err = g.RedisClient.SPopN("errors", step).Result()
		if err != nil {
			break
		}
		errorsList = append(errorsList, errors...)
	}
	return
}
